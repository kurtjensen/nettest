## Net Test - ping multiple addresses for reachability

Program name: nettest.sh


Ask for input config file and then 
ping each ip in the config file to
see if it is reachable.

Show results in colored output based
on status.

==
## Instructions 

Here is a script that can run on the desktop computer ( Linux ) to check multiple points in the network. 

This will be a big help for you to know what to fix when internet goes down.

Most of this you only need to do one time to get it installed.

=============== To Install ( Just one time )=====================

- Download the repo to "/home/your_name"

- This should create a new directory "NetTest" with some files inside.

- Open Terminal program and paste ( Right click  and select paste ) or type this command in the terminal "chmod +x NetTest/nettest.sh". and press return. This will make file executable.

=============== To Use This  =====================

Switch to the NetTest directory to run script. To do this open Terminal program and paste ( Right click  and select paste ) or the following commands in the terminal: 
cd NetTest  (press Enter/Return).
./nettest.sh  (press Enter/Return).

- It will ask you what config file to use for the check.  The config file is just a text file with ips and decriptions that you want to check.  Just hit enter to accept the "home.txt" file as default.
- It should run a script and display a bunch of results to tell you what parts of the network the computer can or can not  reach.
- Red is bad and green is good

There are multiple tests run by this script to several devices on the network.  
I like to order from closest to furthest from computer in the beginning and then I can add a few extra checks.
