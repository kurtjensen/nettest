#!/bin/bash
# Program name: nettest.sh
# Ask for input config file and then 
# ping each ip in the config file to
# see if it is reachable.
#
# Show results in colored output based
# on status.

read -p "What network test file do you want to use [home]: " name
name=${name:-home}
path=$PWD
echo Using config file $path/$name".txt"
echo "=================== Results ===================="

IFS="="
while read -r name value
do
	ping -c 1 "$value" > /dev/null
    if [ $? -eq 0 ]; then
    echo -e "\e[1;32m node $name at $value is up \e[0m"
    else
    echo -e "\e[1;31m node $name at $value is down \e[0m"
    fi
done < $path/$name".txt" 


echo "=================== End Results ===================="
